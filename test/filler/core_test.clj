(ns filler.core-test
  (:require [clojure.test :refer :all]
            [filler.screen :as scr]
            [filler.core :refer :all]))

(deftest point-test
  (testing "Positions in a field"
    (let [field [[1 2 3] [4 5 6] [7 8 9]]]
      (is (= 9 (scr/colour-at field {:x 2 :y 2})))
      (is (= 4 (scr/colour-at field {:x 0 :y 1}))))))
