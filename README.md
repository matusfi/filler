# Filler

Úlohou je naprogramovať funkciu výplne v zjednodušenej verzii skicára. Plocha je
reprezentovaná ako číselné pole screen rozmerov NxM, kde každej farbe zodpovedá
celočíselná hodnota. Treba naprogramovať funkciu: `fill(screen, x, y, color)` Ktorá vyplní `screen`
farbou `color` na súradniciach `x`, `y`. Ľavý horný roh je očíslovaný ako `(0, 0)`.
Vyfarbovanie sa riadi nasledovnými pravidlami:

1. Susediace políčka sú také, ktoré susedia hranou.
2. Ak mali dve susediace políčka rovnakú farbu pred vyfarbením, farba sa vyfarbením jedného z
nich vyleje aj na druhé.
3. Farba sa nemôže vyliať z políčka na susediace políčko, ktoré nemalo rovnakú farbu.
4. Farba sa nevylieva za hranice plániku.

## Príklad:

Vstup:

    0 4 0 0 0 2 0 0 0 0 0 0 1
    0 4 0 0 0 2 0 0 0 0 0 0 1
    0 4 0 0 0 2 0 0 0 0 0 0 1
    0 4 0 0 0 2 7 7 7 7 7 7 1
    0 4 0 0 0 2 0 0 0 0 0 0 1
    0 4 0 0 0 3 0 0 0 0 0 0 1
    0 0 3 3 3 0 0 0 0 0 0 0 1
    0 0 0 0 8 0 0 0 0 0 0 0 1

    fill(2, 1, 5)

Výstup:

    0 4 5 5 5 2 0 0 0 0 0 0 1
    0 4 5 5 5 2 0 0 0 0 0 0 1
    0 4 5 5 5 2 0 0 0 0 0 0 1
    0 4 5 5 5 2 7 7 7 7 7 7 1
    0 4 5 5 5 2 0 0 0 0 0 0 1
    0 4 5 5 5 3 0 0 0 0 0 0 1
    0 0 3 3 3 0 0 0 0 0 0 0 1
    0 0 0 0 8 0 0 0 0 0 0 0 1

## Inštalácia

Stiahni si jar-ko z https://gitlab.com/matusfi/filler/builds/11920833/artifacts/file/target/uberjar/filler-0.1.0-SNAPSHOT-standalone.jar

## Ako na to?

Najprv potrebuješ file, z ktorého si **filler** načíta screen. 
Na testovanie si môžeš stiahnúť niektorý z nasledovných.

- https://gitlab.com/matusfi/filler/raw/master/resources/assignment.screen
- https://gitlab.com/matusfi/filler/raw/master/resources/empty.screen
- https://gitlab.com/matusfi/filler/raw/master/resources/snail.screen

Potom, už len spusti **filler** s nasledovnými parametrami:

    $ java -jar filler-0.1.0-standalone.jar <cesta/k/suboru.screen> <x> <y> <farba>

