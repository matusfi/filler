(ns filler.screen
  (:require [clojure.string :as s :refer [join]]))

(defn colour-at
  "Get the colour at the point. If the point is outside the screen, return nil."
  [screen point]
  (try
     (-> screen
        (nth (:y point))
        (nth (:x point)))
     (catch IndexOutOfBoundsException e nil)))

(defn mk-point
  [x y]
  {:x x :y y})

(defn up
  [point]
  (update-in point [:y] dec))

(defn down
  [point]
  (update-in point [:y] inc))

(defn right
  [point]
  (update-in point [:x] inc))

(defn left
  [point]
  (update-in point [:x] dec))

(defn move
  "Returns a point in the specified direction relative to the current point."
  [direction point]
  (condp = direction
    :up    (up point)
    :down  (down point)
    :right (right point)
    :left  (left point)))

(defn load-screen
  [filename]
  (with-open [rdr (clojure.java.io/reader filename)]
    (vec
        (map
          #(->> %
                (re-seq #"\d+")
                (map read-string)
                (vec))
          (line-seq rdr)))))

(defn print-screen
  [screen]
  (doseq [row screen]
    (println (s/join " " row))))
