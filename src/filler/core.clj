(ns filler.core
  (:require [filler.screen :as scr])
  (:gen-class))

(defn- neighbours-colours
  "Returns a map of neighbouring colours."
  [screen point]
  (let [{x :x y :y} point
        up          (scr/colour-at screen (scr/up point))
        down        (scr/colour-at screen (scr/down point))
        right       (scr/colour-at screen (scr/right point))
        left        (scr/colour-at screen (scr/left point)) ]
    {:up up :down down :left left :right right}))

(defn- same-colour-directions
  "Returns a vector of directions of neighbours with the same colour
  as the current point."
  [screen point desired-colour]
  (let [current-colour (scr/colour-at screen point)]
    (if (= current-colour desired-colour)
      []
      (->> (neighbours-colours screen point)
           (filter #(= current-colour (val %)))
           (keys)))))

(defn- change-colour
  [screen point new-colour]
  (let [coords (map second (reverse (sort-by (comp name key) point)))]
    (assoc-in screen coords new-colour)))

(defn- r-fill
  "Recursive filler function used to modify fill the screen in reference"
  [screen-ref point desired-colour]

  (let [dirs (same-colour-directions @screen-ref point desired-colour)]
    (dosync (alter screen-ref change-colour point desired-colour)) ;; Change the colour of the current point
    (if (empty? dirs)
      screen-ref                ;; If there are no other neighbours with the same colour return the screen
      (doseq [direction dirs]   ;; Else call r-fill on all neighbours with the same colour
        (r-fill screen-ref
                (scr/move direction point)
                desired-colour)))))


(defn fill
   "Fills an area of points of the same colour using these rules:
      1. Susediace políčka sú také, ktoré susedia hranou.
      2. Ak mali dve susediace políčka rovnakú farbu pred vyfarbením,
         farba sa vyfarbením jedného z nich vyleje aj na druhé.
      3. Farba sa nemôže vyliať z políčka na susediace políčko,
         ktoré nemalo rovnakú farbu.
      4. Farba sa nevylieva za hranice plániku.

   Example:
   => (def a-screen [[0 0 0] [0 0 0] [0 0 0]])
   => (fill a-screen {:x 2 :y 5} 3)"
  [screen point desired-colour]
  (let [screen-ref (ref screen)]
    (r-fill screen-ref point desired-colour)
    @screen-ref))

(defn -main
  [& args]
  (let [[filename x-s y-s colour-s] args
        x                       (read-string x-s)
        y                       (read-string y-s)
        colour                  (read-string colour-s)
        screen                  (scr/load-screen filename)]
    (scr/print-screen (fill screen (scr/mk-point x y) colour))))
